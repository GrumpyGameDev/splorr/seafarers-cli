#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "world.h"
#include "dms.h"
#include <math.h>
#include <ctype.h>
#include "common.h"
#include "status.h"

#define STOP_SPEED ("STOP")
#define ONE_THIRD_SPEED ("1/3")
#define TWO_THIRD_SPEED ("2/3")
#define FULL_SPEED ("FULL")
#define FLANK_SPEED ("FLANK")

void handle_heading(WORLD* world, char** next_token)
{

	if (World_is_docked(world))
	{
		printf("\nCannot change heading while docked.\n");
	}
	else
	{
		char* token = strtok_r(0, INPUT_SEPARATORS, next_token);
		if (token)
		{
			DMS dms = { 0 };
			dms.degrees = atoi(token);
			token = strtok_r(0, INPUT_SEPARATORS, next_token);
			if (token)
			{
				dms.minutes = atoi(token);
				token = strtok_r(0, INPUT_SEPARATORS, next_token);
				if (token)
				{
					dms.seconds = (float)atof(token);
				}
			}
			World_set_heading(world, Dms_to_heading(&dms));
			handle_status(world,0);
		}
		else
		{
			printf("\nHEADING degrees [minutes[ seconds]]\n");
		}
	}
}

void handle_speed(WORLD* world, char** next_token)
{
	AVATAR* avatar = World_get_avatar(world);
	if (Avatar_is_docked(avatar))
	{
		printf("\nCannot change speed while docked.\n");
	}
	else
	{
		char* token = strtok_r(0, INPUT_SEPARATORS, next_token);
		if (token)
		{
			if (strcmp(token, STOP_SPEED) == 0)
			{
				Avatar_set_speed(World_get_avatar(world), 0);
				handle_status(world,0);
			}
			else if (strcmp(token, ONE_THIRD_SPEED) == 0)
			{
				Avatar_set_speed(World_get_avatar(world), (float)(1.0 / 3));
				handle_status(world, 0);
			}
			else if (strcmp(token, TWO_THIRD_SPEED) == 0)
			{
				Avatar_set_speed(World_get_avatar(world), (float)(2.0 / 3));
				handle_status(world, 0);
			}
			else if (strcmp(token, FULL_SPEED) == 0)
			{
				Avatar_set_speed(World_get_avatar(world), (float)0.9);
				handle_status(world, 0);
			}
			else if (strcmp(token, FLANK_SPEED) == 0)
			{
				Avatar_set_speed(World_get_avatar(world), (float)1.0);
				handle_status(world, 0);
			}
			else
			{
				printf("\nSPEED [STOP | 1/3 | 2/3 | FULL | FLANK]\n");
			}
		}
		else
		{
			printf("\nSPEED [STOP | 1/3 | 2/3 | FULL | FLANK]\n");
		}
	}
}

void handle_dock(WORLD* world)
{
	switch (World_dock(world))
	{
	case DOCKED:
		printf("\nYou dock.\n");
		handle_status(world, 0);
		break;
	case CANNOT_DOCK:
		printf("\nYou cannot dock at this time.\n");
		handle_status(world, 0);
		break;
	}
}

void handle_undock(WORLD* world)
{
	switch (World_undock(world))
	{
	case UNDOCKED:
		printf("\nYou undock.\n");
		handle_status(world, 0);
		break;
	case NOT_DOCKED:
		printf("\nYou cannot undock at this time.\n");
		handle_status(world, 0);
		break;
	}
}

void handle_move(WORLD* world, char** next_token)
{
	AVATAR* avatar = World_get_avatar(world);
	if (Avatar_is_docked(avatar))
	{
		printf("\nCannot move while docked.\n");
	}
	else
	{
		float x = Avatar_get_x(avatar);
		float y = Avatar_get_y(avatar);
		float heading = Avatar_get_heading(avatar);
		float speed = Avatar_get_speed(avatar);
		x += (float)(speed * cos(heading));
		y += (float)(speed * sin(heading));
		Avatar_set_x(avatar, x);
		Avatar_set_y(avatar, y);
		handle_status(world, 0);
	}
}

void handle_islands(WORLD* world, char** next_token)
{
	AVATAR* avatar = World_get_avatar(world);
	ISLANDS* islands = World_get_islands(world);
	printf("\nKnown Islands:\n");
	int found = 0;
	for (int index = 0; index < Islands_get_count(islands); ++index)
	{
		ISLAND* island = Islands_get_island(islands, index);
		if (Island_get_visits(island))
		{
			found = 1;
			DMS dms = Dms_from_heading(Avatar_heading_to(avatar, Island_get_x(island), Island_get_y(island)));
			printf("%s (%.2f@ %ddegrees %d' %.2f\")\n", Island_get_display_name(island), Avatar_distance_to(avatar,Island_get_x(island),Island_get_y(island)), dms.degrees, dms.minutes, dms.seconds);
		}
	}
	if (!found)
	{
		printf("None");
	}
}

