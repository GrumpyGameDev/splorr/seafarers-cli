#ifndef COMMODITY_H
#define COMMODITY_H

#ifdef __cplusplus
extern "C"{
#endif

typedef enum
{
    COMMODITY_GRAIN,
    COMMODITY_MEAT,
    COMMODITY_WOOD,
	COMMODITY_LIMESTONE,
    COMMODITY_COUNT
} COMMODITY_TYPE;

typedef struct CommodityDescriptor COMMODITY_DESCRIPTOR;

struct CommodityDescriptor
{
	float base_price;//this is the base price for a unit of commodity, all other things being equal
	float supply_factor;//when a unit of commodity is sold by a player, the supply goes up by this amount
	float demand_factor;//when a unit of commodity is purchased by a player, the demand does up by this amount
};

#ifdef __cplusplus
}
#endif

#endif
