#ifndef WORLD_H
#define WORLD_H

#ifdef __cplusplus
extern "C" {
#endif

#include "islands.h"
#include "avatar.h"

typedef struct World WORLD;

struct World 
{
	float dock_distance;
	float visibility;
	AVATAR avatar;
	ISLANDS islands;
};

void World_create(WORLD*, float, float, float);
void World_destroy(WORLD*);
AVATAR* World_get_avatar(WORLD*);
ISLANDS* World_get_islands(WORLD*);

typedef enum 
{
	UNDOCKED,
	CANNOT_UNDOCK,
	NOT_DOCKED
} UNDOCK_RESULT;

typedef enum
{
	DOCKED,
	CANNOT_DOCK
} DOCK_RESULT;

UNDOCK_RESULT World_undock(WORLD*);
int World_can_dock(WORLD*);
DOCK_RESULT World_dock(WORLD*);
float World_get_dock_distance(WORLD*);
float World_get_visibility(WORLD*);
int World_is_docked(WORLD*);
void World_set_heading(WORLD*,float);

#ifdef __cplusplus
}
#endif

#endif
