#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "world.h"
#include "dms.h"
#include <math.h>
#include <ctype.h>

int handle_status_docked(WORLD* world, char** next_token)
{
	printf("\nStatus: Docked\n");
	AVATAR* avatar = World_get_avatar(world);
	ISLAND* island = Islands_get_island(World_get_islands(world), Avatar_get_docked_at(avatar));
	printf("Island name: %s\n", Island_get_display_name(island));
	return 0;
}

int handle_status_at_sea(WORLD* world, char** next_token)
{
	printf("\nStatus: At sea\n");
	AVATAR* avatar = World_get_avatar(world);
	DMS dms = Dms_from_heading(Avatar_get_heading(avatar));
	printf("Heading: %ddegrees %d\' %.2f\"\n", dms.degrees, dms.minutes, dms.seconds);
	printf("Speed: %.2f\n", Avatar_get_speed(avatar));

	ISLANDS* islands = World_get_islands(world);
	float avatar_x = Avatar_get_x(avatar);
	float avatar_y = Avatar_get_y(avatar);
	int found = 0;
	for (int index = 0; index < Islands_get_count(islands); ++index)
	{
		ISLAND* island = Islands_get_island(islands, index);
		if (Island_distance_to(island, avatar_x, avatar_y) <= World_get_dock_distance(world))
		{
			if (found)
			{
				printf("\t%s\n", Island_get_display_name(island));
			}
			else
			{
				printf("Can Dock:\n\t%s\n", Island_get_display_name(island));
				found = 1;
			}
		}
	}
	if (found)
	{
		printf("\n");
	}
	found = 0;
	for (int index = 0; index < Islands_get_count(islands); ++index)
	{
		ISLAND* island = Islands_get_island(islands, index);
		float distance = Island_distance_to(island, avatar_x, avatar_y);
		DMS dms = Dms_from_heading(Avatar_heading_to(avatar, Island_get_x(island), Island_get_y(island)));
		if (distance > World_get_dock_distance(world) && distance <= World_get_visibility(world))
		{
			if (!found)
			{
				printf("Near by:\n");
				found = 1;
			}
			printf("\t%s (%.2f @ %ddegrees %d\' %.2f\")\n", Island_get_display_name(island), distance, dms.degrees, dms.minutes, dms.seconds);
		}
	}
	return 0;
}

int handle_status(WORLD* world, char** next_token)
{
	if (Avatar_is_docked(World_get_avatar(world)))
	{
		return handle_status_docked(world, next_token);
	}
	else
	{
		return handle_status_at_sea(world, next_token);
	}
}
