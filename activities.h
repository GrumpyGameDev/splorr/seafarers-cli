#ifndef ACTIVITIES_H
#define ACTIVITIES_H

#ifdef __cplusplus
extern "C"{
#endif

typedef enum
{
    ACTIVITYTYPE_NONE,
    ACTIVITYTYPE_FORAGING,
    ACTIVITYTYPE_FISHING,
    ACTIVITYTYPE_COUNT
} ACTIVITY_TYPE;

#ifdef __cplusplus
}
#endif

#endif
