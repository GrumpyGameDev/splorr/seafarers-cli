#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "world.h"
#include "dms.h"
#include <math.h>
#include <ctype.h>
#include "help.h"
#include "status.h"
#include "common.h"
#include "navigation.h"
#include "commands.h"

#define INPUT_BUFFER_SIZE (250)
#define DEFAULT_WIDTH (100)
#define DEFAULT_HEIGHT (100)
#define DEFAULT_DISTANCE (10)

void unknown_command()
{
    foreground_color(1);
	printf("\nUnknown command. Try HELP.\n");
    reset_text();
}

int handle_quit(WORLD* world, char** next_token)
{
	foreground_color(4);
	printf("\nBye!\n");
	reset_text();
	return 1;
}

int main(int argc, char** argv)
{
	int done = 0;
	char buffer[INPUT_BUFFER_SIZE];
	char* next_token;
    clear_screen();
    cursor_home();
	foreground_color(1);
    printf("**Seafarers CLI**\n");
    reset_text();
	WORLD world;
	World_create(&world, DEFAULT_WIDTH, DEFAULT_HEIGHT, DEFAULT_DISTANCE);
	while (!done)
	{
        foreground_color(7);
		printf("\n>");
		char* line = fgets(buffer, INPUT_BUFFER_SIZE, stdin);
        reset_text();
		for(char* c=buffer;*c;++c)
		{
			*c=toupper(*c);
		}
		char* token = strtok_r(line, INPUT_SEPARATORS, &next_token);
		if (token)
		{
			if (strcmp(token, QUIT_COMMAND)==0)
			{
				done = handle_quit(&world, &next_token);
			}
			else if (strcmp(token, STATUS_COMMAND) == 0)
			{
				done = handle_status(&world, &next_token);
			}
			else if (strcmp(token, HELP_COMMAND) == 0)
			{
				show_help(&next_token);
			}
			else if (strcmp(token, HEADING_COMMAND) == 0)
			{
				handle_heading(&world, &next_token);
			}
			else if (strcmp(token, SPEED_COMMAND) == 0)
			{
				handle_speed(&world, &next_token);
			}
			else if (strcmp(token, MOVE_COMMAND) == 0)
			{
				handle_move(&world, &next_token);
			}
			else if (strcmp(token, DOCK_COMMAND) == 0)
			{
				handle_dock(&world);
			}
			else if (strcmp(token, UNDOCK_COMMAND) == 0)
			{
				handle_undock(&world);
			}
			else if (strcmp(token, ISLANDS_COMMAND) == 0)
			{
				handle_islands(&world, &next_token);
			}
			else
			{
                unknown_command();
			}
		}
		else
		{
            unknown_command();
		}
	}
    return 0;
}

