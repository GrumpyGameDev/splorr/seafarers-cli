#ifndef DMS_H
#define DMS_H

#ifdef __cplusplus
extern "C" {
#endif

	typedef struct Dms DMS;

	struct Dms
	{
		int degrees;
		int minutes;
		float seconds;
	};

	DMS Dms_from_heading(float);
	float Dms_to_heading(const DMS*);


#ifdef __cplusplus
}
#endif

#endif

