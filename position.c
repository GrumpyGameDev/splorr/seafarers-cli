#include "position.h"
#include <assert.h>

void Position_create(POSITION* position,float x,float y)
{
    assert(position);
    Position_set_x(position,x);
    Position_set_y(position,y);
}

void Position_destroy(POSITION* position)
{
    assert(position);
}

void Position_set_x(POSITION* position,float x)
{
    assert(position);
    position->x = x;
}

void Position_set_y(POSITION* position,float y)
{
    assert(position);
    position->y = y;
}

float Position_get_x(const POSITION* position)
{
    assert(position);
    return position->x;
}

float Position_get_y(const POSITION* position)
{
    assert(position);
    return position->y;
}
