#include "statistics.h"

void Statistic_create(STATISTIC* statistic, int current, int minimum, int maximum)
{
    Statistic_set_minimum(statistic, minimum);
    Statistic_set_maximum(statistic, maximum);
    Statistic_set_current(statistic, current);
}

void Statistic_destroy(STATISTIC* statistic)
{
}

void Statistic_set_current(STATISTIC* statistic, int current)
{
    statistic->current =
        (current < Statistic_get_minimum(statistic)) ? (Statistic_get_minimum(statistic)) :
        (current > Statistic_get_maximum(statistic)) ? (Statistic_get_maximum(statistic)) :
        (current);
}

void Statistic_set_maximum(STATISTIC* statistic, int maximum)
{
    statistic->maximum = maximum;
}

void Statistic_set_minimum(STATISTIC* statistic, int minimum)
{
    statistic->minimum = minimum;
}

int Statistic_get_current(STATISTIC* statistic)
{
    return statistic->current;
}

int Statistic_get_maximum(STATISTIC* statistic)
{
    return statistic->maximum;
}

int Statistic_get_minimum(STATISTIC* statistic)
{
    return statistic->minimum;
}
