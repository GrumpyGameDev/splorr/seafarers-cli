#ifndef STATUS_H
#define STATUS_H

#ifdef __cplusplus
extern "C"{
#endif

#include "world.h"

int handle_status(WORLD* world, char** next_token);

#ifdef __cplusplus
}
#endif

#endif
