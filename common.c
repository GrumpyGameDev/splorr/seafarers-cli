#include "common.h"
#include <stdio.h>

void clear_screen()
{
#ifndef _WIN32
    printf("\033[2J");
#endif
}

void cursor_home()
{
#ifndef _WIN32
	printf("\033[H");
#endif
}

void reset_text()
{
#ifndef _WIN32
	printf("\033[0m");
#endif
}
    
void foreground_color(int color)
{
#ifndef _WIN32
	printf("\033[3%dm",color);
#endif
}

void background_color(int color)
{
#ifndef _WIN32
	printf("\033[4%dm",color);
#endif
}
