#ifndef ISLANDS_H
#define ISLANDS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "island.h"

	typedef struct Islands ISLANDS;

	struct Islands
	{
		int count;
		ISLAND* islands;
	};

	void Islands_create(ISLANDS*, float, float, float);
	void Islands_destroy(ISLANDS*);
	int Islands_get_count(ISLANDS*);
	ISLAND* Islands_get_island(ISLANDS*, int);

#ifdef __cplusplus
}
#endif

#endif

