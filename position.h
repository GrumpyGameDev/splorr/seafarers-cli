#ifndef POSITION_H
#define POSITION_H

#ifdef __cplusplus
extern "C"{
#endif

typedef struct
{
    float x;
    float y;
} POSITION;

void Position_create(POSITION*,float,float);
void Position_destroy(POSITION*);

void Position_set_x(POSITION*,float);
void Position_set_y(POSITION*,float);

float Position_get_x(const POSITION*);
float Position_get_y(const POSITION*);

#ifdef __cplusplus
}
#endif

#endif
