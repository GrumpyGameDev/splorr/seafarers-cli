#include "inventory.h"

void Inventory_create(INVENTORY* inventory)
{
    for(ITEM_TYPE item=0;item<ITEMTYPE_COUNT;++item)
    {
        Inventory_set_count(inventory,item,0);
    }
}

void Inventory_destroy(INVENTORY* inventory)
{
}

int Inventory_get_count(INVENTORY* inventory,ITEM_TYPE item)
{
    return inventory->counts[item];
}

void Inventory_set_count(INVENTORY* inventory,ITEM_TYPE item,int count)
{
    inventory->counts[item]=count;
}

