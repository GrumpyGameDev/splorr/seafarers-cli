#ifndef COMMON_H
#define COMMON_H

#ifdef __cplusplus
extern "C" {
#endif

#define INPUT_SEPARATORS (" \n\t")

void clear_screen();
void cursor_home();
void reset_text();
void foreground_color(int);
void background_color(int);

#ifdef _WIN32
#define strtok_r strtok_s
#endif

#ifdef __cplusplus
}
#endif

#endif
