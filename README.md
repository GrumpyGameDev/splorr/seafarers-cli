# Seafarers CLI

## Basic Game

The general gist is that you have a boat and travel from island to island for fun and profit.

It is also a survival game. Your avatar gets hungry, and when hungry enough, he will lose health until he dies.

Directly or indirectly the player deals with trading commodities.

Every commodity in the game has a particular supply and demand level at each island, and these levels vary independently.

It is best to buy a commodity where supply is high and demand is low.

It is best to sell a commodity where demand is high and supply is low.

However, buying a commodity increases demand, and selling a commodity increases supply, so eventually through the actions of the player, the prices at the various islands will level out.

Also, when a player sells a commodity, there is a "discount rate" applied by the seller (who has to make money on the transaction, after all).

## TODO

- [ ] commodities
- [ ] food
- [ ] fishing
- [ ] fights
- [ ] gambling
- [ ] save
- [ ] load
- [ ] foraging

## Commands and Business Rules Concerning Them

### QUIT

The player may quit the game at any time.

Should there be a prompt to save?

### STATUS

The player may get his status at any time.

### HELP

The player may get help at any time.

### HEADING

The player may change heading whenever at sea and not engaged in an activity.

### SPEED

The player may change speed whenever at sea and not engaged in an activity.

### MOVE

The player may move whenever at sea and not engaged in an activity.

### DOCK

The player may dock whenever at sea and close enough to an island (<=1.0) and not engaged in an activity.

### UNDOCK

The player may undock whenever docked and not engaged in an activity.

### ISLANDS

The player may get a list of islands at any time.

### ACTIVITIES

Lists the activities available to the player in the current context.

### PRICE

### BUY

### SELL

### GAMBLE

### FISH

### FORAGE

### JOBS

### DELIVER


