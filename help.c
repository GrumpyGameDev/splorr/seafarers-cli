#include <stdio.h>
#include "help.h"
#include "common.h"
#include <string.h>
#include "commands.h"

void show_help(char** next_token)
{
	char* token = strtok_r(0, INPUT_SEPARATORS, next_token);
	if (token)
	{
		if (strcmp(token, QUIT_COMMAND) == 0)
		{
			printf("\nQUIT:\n\tTo no one's surprise, this quits the game.\n");
		}
		else if (strcmp(token, DOCK_COMMAND) == 0)
		{
			printf("\nDOCK:\n\tWhen less than 1 away from an island, causes you to dock at the island. Increases the visit count for the island, and discovers the island's name.\n");
		}
		else if (strcmp(token, HEADING_COMMAND) == 0)
		{
			printf("\nHEADING [d] [m] [s]:\n\tSets new heading to degrees minutes and seconds. Minutes and seconds are optional.\n");
		}
		else
		{
			printf("\nUnknown command.\n");
		}
	}
	else
	{
		printf("\nHelp:\n");
		printf("\tQUIT\n");
		printf("\tHELP\n");
		printf("\tSTATUS\n");
		printf("\tHEADING\n");
		printf("\tSPEED\n");
		printf("\tMOVE\n");
		printf("\tISLANDSn");
	}
}


