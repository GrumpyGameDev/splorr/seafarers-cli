#include "dms.h"
#include <math.h>

DMS Dms_from_heading(float heading)
{
	float quarter_pi = atan2f(1, 1);
	heading = atan2f(sinf(heading), cosf(heading));
	if (heading < 0)
	{
		heading += quarter_pi * 8;
	}
	float value = heading * 45 / quarter_pi;
	DMS result;
	result.degrees = (int)value;
	value -= result.degrees;
	value *= 60;
	result.minutes = (int)value;
	value -= result.minutes;
	result.seconds = value * 60;
	return result;
}

float Dms_to_heading(const DMS* dms)
{
	float quarter_pi = atan2f(1, 1);
	return ((dms->seconds / 60 + dms->minutes) / 60 + dms->degrees) * quarter_pi / 45;
}
