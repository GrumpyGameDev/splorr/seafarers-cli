#ifndef AVATAR_H
#define AVATAR_H

#ifdef __cplusplus
extern "C" {
#endif

#include "inventory.h"
#include "statistics.h"
#include "activities.h"
#include "position.h"

typedef struct Avatar AVATAR;

struct Avatar
{
	POSITION position;
	float heading;
	float speed;
	int docked_at;
    ACTIVITY_TYPE activity;
    float money;
    INVENTORY inventory;
    STATISTIC statistics[STATISTICTYPE_COUNT];
};

void Avatar_create(AVATAR*);
void Avatar_destroy(AVATAR*);
int Avatar_is_docked(AVATAR*);
int Avatar_get_docked_at(AVATAR*);
void Avatar_set_docked_at(AVATAR*,int);
void Avatar_set_activity(AVATAR*, ACTIVITY_TYPE);
ACTIVITY_TYPE Avatar_get_activity(AVATAR*);
POSITION* Avatar_get_position(AVATAR*);
float Avatar_get_x(AVATAR*);
float Avatar_get_y(AVATAR*);
void Avatar_set_x(AVATAR*,float);
void Avatar_set_y(AVATAR*,float);
float Avatar_get_heading(AVATAR*);
void Avatar_set_heading(AVATAR*,float);
float Avatar_get_speed(AVATAR*);
void Avatar_set_speed(AVATAR*,float);
float Avatar_distance_to(AVATAR*, float, float);
float Avatar_heading_to(AVATAR*, float, float);
INVENTORY*  Avatar_get_inventory(AVATAR*);
STATISTIC* Avatar_get_statistic(AVATAR*, STATISTIC_TYPE);

#ifdef __cplusplus
}
#endif

#endif
