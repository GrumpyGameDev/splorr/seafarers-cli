#ifndef ISLAND_H
#define ISLAND_H

#ifdef __cplusplus
extern "C" {
#endif

#define ISLAND_NAME_LENGTH (30)

typedef struct Island ISLAND;

struct Island
{
	char name[ISLAND_NAME_LENGTH];
	float x;
	float y;
	int visits;
};

void Island_create(ISLAND*);
void Island_destroy(ISLAND*);
void Island_set_x(ISLAND*, float);
void Island_set_y(ISLAND*, float);
float Island_get_x(ISLAND*);
float Island_get_y(ISLAND*);
const char* Island_get_name(ISLAND*);
const char* Island_get_display_name(ISLAND*);
void Island_set_name(ISLAND*, const char*);
float Island_distance_to(ISLAND*, float, float);
void Island_generate_name(ISLAND*);
int Island_get_visits(ISLAND*);
void Island_set_visits(ISLAND*,int);
void Island_increment_visits(ISLAND*);

#ifdef __cplusplus
}
#endif

#endif

