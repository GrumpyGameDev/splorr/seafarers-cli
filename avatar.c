#include "avatar.h"
#include <memory.h>
#include <math.h>
#include <assert.h>

static int statistic_minimums[STATISTICTYPE_COUNT]=
{
    0,//health
    0//satiety
};
static int statistic_maximums[STATISTICTYPE_COUNT]=
{
    100,//health
    100//satiety
};
static int statistic_defaults[STATISTICTYPE_COUNT]=
{
    100,//health
    100//satiety
};
static int inventory_defaults[ITEMTYPE_COUNT]=
{
    100,//rations
	0,//cockle
	0,//mussel
	0//oyster
};

POSITION* Avatar_get_position(AVATAR* avatar)
{
	assert(avatar);
	return &avatar->position;
}

void Avatar_create(AVATAR* avatar)
{
	assert(avatar);
	memset(avatar, 0, sizeof(AVATAR));
	avatar->docked_at = -1;
	avatar->heading = 0;
	avatar->speed = 1;
	Position_create(Avatar_get_position(avatar), 0, 0);
    avatar->activity = ACTIVITYTYPE_NONE;
    Inventory_create(Avatar_get_inventory(avatar));
    for(ITEM_TYPE item=0;item<ITEMTYPE_COUNT;++item)
    {
        Inventory_set_count(Avatar_get_inventory(avatar),item,inventory_defaults[item]);
    }
    for(STATISTIC_TYPE statistic=0;statistic<STATISTICTYPE_COUNT;++statistic)
    {
        Statistic_create(Avatar_get_statistic(avatar,statistic), statistic_defaults[statistic], statistic_minimums[statistic], statistic_maximums[statistic]);
    }
}

void Avatar_destroy(AVATAR* avatar)
{
	assert(avatar);
	Inventory_destroy(Avatar_get_inventory(avatar));
	for (STATISTIC_TYPE statistic = 0; statistic < STATISTICTYPE_COUNT; ++statistic)
	{
		Statistic_destroy(Avatar_get_statistic(avatar, statistic));
	}
}

int Avatar_is_docked(AVATAR* avatar)
{
	assert(avatar);
	return avatar->docked_at != -1;
}

float Avatar_get_x(AVATAR* avatar)
{
	assert(avatar);
	return Position_get_x(Avatar_get_position(avatar));
}

float Avatar_get_y(AVATAR* avatar)
{
	assert(avatar);
	return Position_get_y(Avatar_get_position(avatar));
}

float Avatar_get_heading(AVATAR* avatar)
{
	assert(avatar);
	return avatar->heading;
}

float Avatar_get_speed(AVATAR* avatar)
{
	assert(avatar);
	return avatar->speed;
}

int Avatar_get_docked_at(AVATAR* avatar)
{
	assert(avatar);
	return avatar->docked_at;
}

void Avatar_set_x(AVATAR* avatar, float x)
{
	assert(avatar);
	Position_set_x(Avatar_get_position(avatar), x);
}

void Avatar_set_y(AVATAR* avatar, float y)
{
	assert(avatar);
	Position_set_y(Avatar_get_position(avatar), y);
}

void Avatar_set_heading(AVATAR* avatar, float heading)
{
	assert(avatar);
	avatar->heading = heading;
}

void Avatar_set_speed(AVATAR* avatar, float speed)
{
	assert(avatar);
	avatar->speed = speed;
}
float Avatar_distance_to(AVATAR* avatar, float x, float y)
{
	assert(avatar);
	float delta_x = Avatar_get_x(avatar) - x;
	float delta_y = Avatar_get_y(avatar) - y;
	return sqrtf(delta_x * delta_x + delta_y * delta_y);
}

float Avatar_heading_to(AVATAR* avatar, float x, float y)
{
	assert(avatar);
	return atan2f(y - Avatar_get_y(avatar), x - Avatar_get_x(avatar));
}

void Avatar_set_docked_at(AVATAR* avatar, int docked_at)
{
	assert(avatar);
	avatar->docked_at = docked_at;
}

INVENTORY* Avatar_get_inventory(AVATAR* avatar)
{
	assert(avatar);
	return &avatar->inventory;
}

STATISTIC* Avatar_get_statistic(AVATAR* avatar, STATISTIC_TYPE statistic)
{
	assert(avatar);
	return &avatar->statistics[statistic];
}

void Avatar_set_activity(AVATAR* avatar, ACTIVITY_TYPE activity)
{
	assert(avatar);
	avatar->activity = activity;
}

ACTIVITY_TYPE Avatar_get_activity(AVATAR* avatar)
{
	assert(avatar);
	return avatar->activity;
}
