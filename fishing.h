#ifndef FISHING_H
#define FISHING_H

#ifdef __cplusplus
extern "C"{
#endif

typedef enum
{
    FISHTYPE_ALBACORE,
    FISHTYPE_TUNA,
    FISHTYPE_MARLIN,
    FISHTYPE_COUNT;
} FISH_TYPE;

typedef struct
{
    FISH_TYPE fish;
    float x;
    float y;
    float radius;
} FISHINGSPOT;

typedef struct
{
    int count;
    FISHINGSPOT* fishing_spots;
} FISHINGSPOTS;

#ifdef __cplusplus
}
#endif

#endif
