#include "island.h"
#include <memory.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

void Island_create(ISLAND* island)
{
	memset(island, 0, sizeof(ISLAND));
}
void Island_destroy(ISLAND* island)
{

}
void Island_set_x(ISLAND* island, float x)
{
	island->x = x;
}
void Island_set_y(ISLAND* island, float y)
{
	island->y = y;
}
float Island_get_x(ISLAND* island)
{
	return island->x;
}
float Island_get_y(ISLAND* island)
{
	return island->y;
}
static const char* unknown_island_name = "UNKNOWN";
const char* Island_get_display_name(ISLAND* island)
{
	if (Island_get_visits(island)>0)
	{
		return island->name;
	}
	else
	{
		return unknown_island_name;
	}
}
const char* Island_get_name(ISLAND* island)
{
	return island->name;
}
void Island_set_name(ISLAND* island, const char* name)
{
	strncpy(island->name, name, ISLAND_NAME_LENGTH * sizeof(char));
}
float Island_distance_to(ISLAND* island, float x, float y)
{
	float delta_x = Island_get_x(island) - x;
	float delta_y = Island_get_y(island) - y;
	return sqrtf(delta_x*delta_x + delta_y * delta_y);
}

static char* vowels="AEIOU";
static char* consonants="HKLMP";

void Island_generate_name(ISLAND* island)
{
    int length = rand()%3+rand()%3+rand()%3+3;
    int vowel = rand()%2;
    memset(island->name,0,ISLAND_NAME_LENGTH);
    int index=0;
    while(length>0)
    {
        if(vowel)
        {
            island->name[index++]=vowels[rand()%strlen(vowels)];
        }
        else
        {
            island->name[index++]=consonants[rand()%strlen(consonants)];
        }
        vowel=!vowel;
        length--;
    }
}

int Island_get_visits(ISLAND* island)
{
    return island->visits;
}

void Island_set_visits(ISLAND* island,int count)
{
    island->visits = count;
}

void Island_increment_visits(ISLAND* island)
{
	Island_set_visits(island, Island_get_visits(island) + 1);
}
