#ifndef NAVIGATION_H
#define NAVIGATION_H

#ifdef __cplusplus
extern "C"{
#endif

#include "world.h"

void handle_heading(WORLD*, char**);
void handle_speed(WORLD*, char**);
void handle_dock(WORLD*);
void handle_undock(WORLD*);
void handle_move(WORLD*, char**);
void handle_islands(WORLD*, char**);

#ifdef __cplusplus
}
#endif

#endif
