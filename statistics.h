#ifndef STATISTICS_H
#define STATISTICS_H

#ifdef __cplusplus
extern "C"{
#endif

typedef enum
{
    STATISTICTYPE_HEALTH,
    STATISTICTYPE_SATIETY,
    STATISTICTYPE_COUNT
} STATISTIC_TYPE;

typedef struct Statistic STATISTIC;

struct Statistic
{
    int current;
    int maximum;
    int minimum;
};

void Statistic_create(STATISTIC*, int, int, int);
void Statistic_destroy(STATISTIC*);
void Statistic_set_current(STATISTIC*, int);
void Statistic_set_maximum(STATISTIC*, int);
void Statistic_set_minimum(STATISTIC*, int);
int Statistic_get_current(STATISTIC*);
int Statistic_get_maximum(STATISTIC*);
int Statistic_get_minimum(STATISTIC*);

#ifdef __cplusplus
}
#endif

#endif
