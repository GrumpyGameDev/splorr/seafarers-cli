#include "islands.h"
#include <memory.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdio.h>

void Islands_create(ISLANDS* islands, float world_width, float world_height, float minimum_distance)
{
	int count = (int)((world_width * world_height) / (atan2f(1, 1) * 4 * minimum_distance * minimum_distance));
	islands->count = count;
	islands->islands = calloc(sizeof(ISLAND), count);
	for (int index = 0; index < count; ++index)
	{
		ISLAND* island = Islands_get_island(islands, index);
		Island_create(island);
	}
	int current = 0;
	while (current < count)
	{
		float x = ((float)rand() / RAND_MAX) * world_width;
		float y = ((float)rand() / RAND_MAX) * world_height;
		int found = 0;
		for (int index = 0; !found && index < current; ++index)
		{
			ISLAND* island = Islands_get_island(islands, index);
			if (Island_distance_to(island, x, y) < minimum_distance)
			{
				found = 1;
			}
		}
		if (!found)
		{
			Island_set_x(Islands_get_island(islands, current), x);
			Island_set_y(Islands_get_island(islands, current), y);
			++current;
		}
	}
    current=0;
    while(current<count)
    {
        Island_generate_name(Islands_get_island(islands,current));
        int found=0;
        for(int index=0;!found && index<current;++index)
        {
            if(strcmp(Island_get_name(Islands_get_island(islands,current)),Island_get_name(Islands_get_island(islands,index)))==0)
            {
                found = 1;
            }
        }
        if(!found)
        {
            current++;
        }
    }
}

void Islands_destroy(ISLANDS* islands)
{
	if (islands->islands)
	{
		for (int index = 0; index < Islands_get_count(islands); ++index)
		{
			Island_destroy(Islands_get_island(islands, index));
		}
		free(islands->islands);
	}
	islands->islands = 0;
	islands->count = 0;
}

int Islands_get_count(ISLANDS* islands)
{
	return islands->count;
}

ISLAND* Islands_get_island(ISLANDS* islands, int index)
{
	return &islands->islands[index];
}


