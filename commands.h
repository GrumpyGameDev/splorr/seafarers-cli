#ifndef COMMANDS_H
#define COMMANDS_H

#ifdef __cplusplus
extern "C" {
#endif

#define QUIT_COMMAND ("QUIT")
#define STATUS_COMMAND ("STATUS")
#define HELP_COMMAND ("HELP")
#define HEADING_COMMAND ("HEADING")
#define SPEED_COMMAND ("SPEED")
#define MOVE_COMMAND ("MOVE")
#define DOCK_COMMAND ("DOCK")
#define UNDOCK_COMMAND ("UNDOCK")
#define ISLANDS_COMMAND ("ISLANDS")

#ifdef __cplusplus
}
#endif

#endif
