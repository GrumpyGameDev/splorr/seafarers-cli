#include "world.h"
#include <memory.h>

AVATAR* World_get_avatar(WORLD* world)
{
	return &world->avatar;
}

void World_create(WORLD* world,float world_width,float world_height, float minimum_distance)
{
	memset(world, 0, sizeof(WORLD));
	Avatar_create(World_get_avatar(world));
	Islands_create(World_get_islands(world),world_width,world_height,minimum_distance);
	Avatar_set_x(World_get_avatar(world), world_width / 2);
	Avatar_set_y(World_get_avatar(world), world_height / 2);
	world->dock_distance = 1;
	world->visibility = 10;
}

void World_destroy(WORLD* world)
{
	Avatar_destroy(World_get_avatar(world));
}

ISLANDS* World_get_islands(WORLD* world)
{
	return &world->islands;
}

UNDOCK_RESULT World_undock(WORLD* world)
{
	AVATAR* avatar = World_get_avatar(world);
	if (Avatar_is_docked(avatar))
	{
		if (Avatar_get_activity(avatar) == ACTIVITYTYPE_NONE)
		{
			ISLAND* island = Islands_get_island(World_get_islands(world), Avatar_get_docked_at(avatar));
			Avatar_set_x(avatar, Island_get_x(island));
			Avatar_set_y(avatar, Island_get_y(island));
			Avatar_set_docked_at(avatar, -1);
			return UNDOCKED;
		}
		else
		{
			return CANNOT_UNDOCK;
		}
	}
	else
	{
		return NOT_DOCKED;
	}
}

int World_can_dock(WORLD* world)
{
	AVATAR* avatar = World_get_avatar(world);
	if (Avatar_is_docked(avatar) || Avatar_get_activity(avatar)==ACTIVITYTYPE_NONE)
	{
		return 0;
	}
	else
	{
		ISLANDS* islands = World_get_islands(world);
		for (int index = 0; index < Islands_get_count(islands); ++index)
		{
			float distance = Island_distance_to(Islands_get_island(islands, index), Avatar_get_x(avatar), Avatar_get_y(avatar));
			if (distance <= World_get_dock_distance(world))
			{
				return 1;
			}
		}
	}
	return 0;
}

int World_is_docked(WORLD* world)
{
    return Avatar_is_docked(World_get_avatar(world));
}

void World_set_heading(WORLD* world,float heading)
{
    Avatar_set_heading(World_get_avatar(world),heading);
}

DOCK_RESULT World_dock(WORLD* world)
{
	if (World_can_dock(world))
	{
		AVATAR* avatar = World_get_avatar(world);
		ISLANDS* islands = World_get_islands(world);
		for (int index = 0; index < Islands_get_count(islands); ++index)
		{
			ISLAND* island = Islands_get_island(islands, index);
			float distance = Island_distance_to(island, Avatar_get_x(avatar), Avatar_get_y(avatar));
			if (distance <= World_get_dock_distance(world))
			{
				Avatar_set_docked_at(avatar, index);
				Island_increment_visits(island);
				return DOCKED;
			}
		}
		return CANNOT_DOCK;
	}
	else
	{
		return CANNOT_DOCK;
	}
}

float World_get_dock_distance(WORLD* world)
{
	return world->dock_distance;
}

float World_get_visibility(WORLD* world)
{
	return world->visibility;
}
