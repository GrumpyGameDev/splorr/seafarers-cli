#ifndef INVENTORY_H
#define INVENTORY_H

#ifdef __cplusplus
extern "C"{
#endif

typedef enum
{
    ITEMTYPE_RATIONS,
	ITEMTYPE_COCKLE,
	ITEMTYPE_MUSSEL,
	ITEMTYPE_OYSTER,
    ITEMTYPE_COUNT
} ITEM_TYPE;

typedef struct Inventory INVENTORY;

struct Inventory
{
    int counts[ITEMTYPE_COUNT];
};

void Inventory_create(INVENTORY*);
void Inventory_destroy(INVENTORY*);
int Inventory_get_count(INVENTORY*,ITEM_TYPE);
void Inventory_set_count(INVENTORY*,ITEM_TYPE,int);

#ifdef __cplusplus
}
#endif

#endif
